package com.iceberg.tlm;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Helper {

	private static Helper instance = new Helper();

	public static final String INTERNAL_RESOURCE_DIR = "resources";

	public static final String SOUNDS_DIR = "sounds";

	public static final String SOUNDS_EXTENSION = ".mp3";

	public static InputStream getInternalResourceInputStream(
			String aboutResourcesPath) {
		return getInstance().getResourceAsStreamByInstance(
				getResourcePath(aboutResourcesPath));
	}

	public static URL getInternalResource(String aboutResourcesPath) {
		return getInstance().getResourceByInstance(
				getResourcePath(aboutResourcesPath));
	}

	public static String getResourcePath(String aboutResourcesPath) {
		return File.separator + INTERNAL_RESOURCE_DIR + File.separator
				+ aboutResourcesPath;
	}

	public static String readStream(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder(512);
		Reader r = new InputStreamReader(is, "UTF-8");
		int c = 0;
		while ((c = r.read()) != -1) {
			sb.append((char) c);
		}
		return sb.toString();
	}

	public static String getInternalResourceAsString(String aboutResourcesPath) {
		try {
			return readStream(getInternalResourceInputStream(aboutResourcesPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	private static Helper getInstance() {
		return instance;
	}

	public static URL getResource(String path) {
		return getInstance().getResourceByInstance(path);
	}

	public static List<String> listInternalFiles(String internalWordsFolder,
			final String extension) {
		String absPath = File.separator + INTERNAL_RESOURCE_DIR
				+ File.separator + internalWordsFolder;
		URL url = getResource(absPath);
		try {
			File afiles = new File(url.toURI());
			File[] files = afiles.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.isFile()
							&& pathname.getName().endsWith(extension);
				}
			});
			List<String> fileNames = new ArrayList<String>(files.length);
			for (File file : files) {
				fileNames.add(file.getName());
			}
			return fileNames;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	private URL getResourceByInstance(String path) {
		return this.getClass().getResource(path);
	}

	private InputStream getResourceAsStreamByInstance(String path) {
		return this.getClass().getResourceAsStream(path);
	}

}
