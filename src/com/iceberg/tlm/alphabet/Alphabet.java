package com.iceberg.tlm.alphabet;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.iceberg.tlm.AlphabetConfig;
import com.iceberg.tlm.Helper;
import com.iceberg.tlm.cases.sets.AlphabetCasePackage;

public class Alphabet {

	public static final String ALPHABET_DIR = "alphabet";

	public static final String ALPHABET_SOUNDS_DIR = ALPHABET_DIR
			+ File.separator + Helper.SOUNDS_DIR;

	public static final String CONSONANTS_STRING = "consonants";

	public static final String VOWELS_STRING = "vowels";

	public static final String CONSONANTS_SOUNDS_DIR = Alphabet.ALPHABET_SOUNDS_DIR
			+ File.separator + CONSONANTS_STRING;

	public static final String VOWELS_SOUNDS_DIR = Alphabet.ALPHABET_SOUNDS_DIR
			+ File.separator + VOWELS_STRING;

	private AlphabetConfig config;

	private Symbol[] symbols;

	private Symbol[] consonants;

	private Symbol[] lowConsonants;

	private Symbol[] middleConsonants;

	private Symbol[] highConsonants;

	private Symbol[] vowels;

	private Symbol[] shortVowels;

	private Symbol[] longVowels;

	private Symbol[] baseLowConsonants;

	private Symbol[] additionalLowConsonants;

	private Symbol[] baseLowNotOralAndNazalConsonants;

	private Symbol[] baseLowOralAndNazalConsonants;

	private Map<String, Symbol> signToSymbolMap;

	private AlphabetCasePackage alphabetCasePackage;

	public Alphabet() {
		this.config = new AlphabetConfig();
		Symbol symbol1 = new Symbol(1, this, "พ", Symbol.CONSONANT, Symbol.LOW,
				Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN, "п", "p",
				"пхоо", "phoo", "Произносится с придыханием.", "พาน", 30);
		Symbol symbol2 = new Symbol(2, this, "ท", Symbol.CONSONANT, Symbol.LOW,
				Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN, "т", "t",
				"тхоо", "thoo", "Произносится с придыханием.", "ทหาร", 23);
		Symbol symbol3 = new Symbol(3, this, "ช", Symbol.CONSONANT, Symbol.LOW,
				Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN, "ч", "ch",
				"чоо", "choo",
				"В русском языке \"ч\" уже произносится с придыханием.",
				"ช้าง", 10);
		Symbol symbol4 = new Symbol(4, this, "ค", Symbol.CONSONANT, Symbol.LOW,
				Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN, "к", "k",
				"кхоо", "khoo", "Произносится с придыханием.", "ควาย", 4);
		Symbol symbol5 = new Symbol(5, this, "ฟ", Symbol.CONSONANT, Symbol.LOW,
				Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN, "ф", "f",
				"фоо", "foo", null, "ฟัน", 31);
		Symbol symbol6 = new Symbol(6, this, "ซ", Symbol.CONSONANT, Symbol.LOW,
				Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN, "с", "s",
				"соо", "soo", null, "โซ่", 11);
		Symbol symbol7 = new Symbol(7, this, "ฮ", Symbol.CONSONANT, Symbol.LOW,
				Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN, "х", "h",
				"хоо", "hoo", null, "นกฮูก", 44);
		Symbol symbol8 = new Symbol(8, this, "ม", Symbol.CONSONANT, Symbol.LOW,
				Symbol.SONORANT, Symbol.NAZAL, Symbol.UNKNOWN, "м", "m", "моо",
				"moo", null, "ม้า", 33);
		Symbol symbol9 = new Symbol(9, this, "น", Symbol.CONSONANT, Symbol.LOW,
				Symbol.SONORANT, Symbol.NAZAL, Symbol.UNKNOWN, "н", "n", "ноо",
				"hoo", null, "หนู", 25);
		Symbol symbol10 = new Symbol(10, this, "ง", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.NAZAL, Symbol.UNKNOWN,
				"нг", "ng", "нгоо", "ngoo", "Почти как английский \"ng.\"",
				"งู", 7);
		Symbol symbol11 = new Symbol(11, this, "ว", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.ORAL, Symbol.UNKNOWN, "в",
				"w", "воо", "woo", "Произностися как английский \"w\"", "แหวน",
				37);
		Symbol symbol12 = new Symbol(12, this, "ล", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.ORAL, Symbol.UNKNOWN, "л",
				"l", "лоо", "loo", null, "ลิง", 36);
		Symbol symbol13 = new Symbol(13, this, "ร", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.ORAL, Symbol.UNKNOWN, "р",
				"r", "роо", "roo", null, "เรือ", 35);
		Symbol symbol14 = new Symbol(14, this, "ย", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.ORAL, Symbol.UNKNOWN, "й",
				"i", "йоо", "ioo", null, "ยักษ์", 34);
		Symbol symbol15 = new Symbol(15, this, "-ี", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"и", "i", "ии", "ii", null, null, 4);
		Symbol symbol16 = new Symbol(16, this, "-ู", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"у", "u", "уу", "uu", null, null, 8);
		Symbol symbol17 = new Symbol(17, this, "-า", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"а", "a", "аа", "aa", null, null, 2);
		Symbol symbol18 = new Symbol(18, this, "-อ", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"о", "o", "оо", "oo", null, null, 16);
		Symbol symbol19 = new Symbol(19, this, "เ-", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"e", "e", "ее", "ee",
				"Произносится магко, без начальной \"й\".", null, 10);
		Symbol symbol20 = new Symbol(20, this, "แ-", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"э", "e", "ээ", "ee", null, null, 12);
		Symbol symbol21 = new Symbol(
				21,
				this,
				"โ-",
				Symbol.VOWEL,
				Symbol.UNKNOWN,
				Symbol.UNKNOWN,
				Symbol.UNKNOWN,
				Symbol.LONG,
				"о",
				"o",
				"оо",
				"oo",
				"Произносится как \"о\", но с вытягиванием и округлением губ, как \"у\".",
				null, 14);
		Symbol symbol22 = new Symbol(22, this, "-ือ", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"ы", "i", "ыы", "ii", null, null, 6);
		Symbol symbol23 = new Symbol(23, this, "เ-อ", Symbol.VOWEL,
				Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG,
				"ё", "eo", "ео", "eo",
				"Произносится, как в английском слове \"her\".", null, 18);
		Symbol symbol24 = new Symbol(24, this, "ภ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"п", "p", "пхоо", "phoo", "Произносится с придыханием.",
				"สำเภา", 32);
		Symbol symbol25 = new Symbol(25, this, "ฑ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"т", "t", "тхоо", "thoo", "Произносится с придыханием.",
				"มณโฑ", 17);
		Symbol symbol26 = new Symbol(26, this, "ฒ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"т", "t", "тхоо", "thoo", "Произносится с придыханием.",
				"ผู้เฒ่า", 18);
		Symbol symbol27 = new Symbol(27, this, "ธ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"т", "t", "тхоо", "thoo", "Произносится с придыханием.", "ธง",
				24);
		Symbol symbol28 = new Symbol(28, this, "ฌ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"ч", "ch", "чоо", "choo",
				"В русском языке \"ч\" уже произносится с придыханием.", "เฌอ",
				12);
		Symbol symbol29 = new Symbol(29, this, "ฅ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"к", "k", "кхоо", "khoo", "Произносится с придыханием.", "คน",
				5);
		Symbol symbol30 = new Symbol(30, this, "ฆ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"к", "k", "кхоо", "khoo", "Произносится с придыханием.",
				"ระฆัง", 6);
		Symbol symbol31 = new Symbol(31, this, "ณ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.NAZAL, Symbol.UNKNOWN, "н",
				"n", "ноо", "noo", null, "เณร", 19);
		Symbol symbol32 = new Symbol(32, this, "ฬ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.ORAL, Symbol.UNKNOWN, "л",
				"l", "лоо", "loo", null, "จุฬา", 42);
		Symbol symbol33 = new Symbol(33, this, "ญ", Symbol.CONSONANT,
				Symbol.LOW, Symbol.SONORANT, Symbol.ORAL, Symbol.UNKNOWN, "й",
				"io", "йоо", "ioo", null, "หญิง", 13);

		Symbol symbol34 = new Symbol(34, this, "ผ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"п", "p", "пхоо", "phoo", null, "ผึ้ง", 28);
		Symbol symbol35 = new Symbol(35, this, "ถ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"т", "t", "тхоо", "thoo", null, "ถุง", 22);
		Symbol symbol36 = new Symbol(36, this, "ฐ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"т", "t", "тхоо", "thoo", null, "ฐาน", 16);
		Symbol symbol37 = new Symbol(37, this, "ฉ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"ч", "ch", "чоо", "choo", null, "ฉิ่ง", 9);
		Symbol symbol38 = new Symbol(38, this, "ข", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"к", "k", "кхоо", "khoo", null, "ไข่", 2);
		Symbol symbol39 = new Symbol(39, this, "ฃ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"к", "k", "кхоо", "khoo", null, "ขวด", 3);
		Symbol symbol40 = new Symbol(40, this, "ฝ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"ф", "f", "фоо", "foo", null, "ฝา", 29);
		Symbol symbol41 = new Symbol(41, this, "ส", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"с", "s", "соо", "soo", null, "เสือ", 40);
		Symbol symbol42 = new Symbol(42, this, "ศ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"с", "s", "соо", "soo", null, "ศาลา", 38);
		Symbol symbol43 = new Symbol(43, this, "ษ", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"с", "s", "соо", "soo", null, "ฤๅษี", 39);
		Symbol symbol44 = new Symbol(44, this, "ห", Symbol.CONSONANT,
				Symbol.HIGH, Symbol.SPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"х", "h", "хоо", "hoo", null, "หีบ", 41);

		Symbol symbol45 = new Symbol(45, this, "บ", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"б", "b", "боо", "boo", null, "ใบไม้", 26);
		Symbol symbol46 = new Symbol(46, this, "ป", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"п", "p", "поо", "poo", null, "ปลา", 27);
		Symbol symbol47 = new Symbol(47, this, "ด", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"д", "d", "доо", "doo", null, "เด็ก", 20);
		Symbol symbol48 = new Symbol(48, this, "ฎ", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"д", "d", "доо", "doo", null, "ชฎา", 14);
		Symbol symbol49 = new Symbol(49, this, "ต", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"т", "t", "тоо", "too", null, "เต่า", 21);
		Symbol symbol50 = new Symbol(50, this, "ฏ", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"т", "t", "тоо", "too", null, "ปฏัก", 15);
		Symbol symbol51 = new Symbol(51, this, "จ", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"ть", "t", "тьоо", "too", null, "จาน", 8);
		Symbol symbol52 = new Symbol(52, this, "ก", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"к", "k", "коо", "koo", null, "ไก่", 1);
		Symbol symbol53 = new Symbol(53, this, "อ", Symbol.CONSONANT,
				Symbol.MIDDLE, Symbol.ASPIRANT, Symbol.UNKNOWN, Symbol.UNKNOWN,
				"о", " о", "оо", "oo", null, "ไก่", 43);

		// гласные-кластеры
		/*
		 * Symbol symbol54 = new Symbol(this, "เ-ีย", Symbol.VOWEL,
		 * Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG, "ииа",
		 * "iia", "ииа", "iia", null, null, ); Symbol symbol55 = new
		 * Symbol(this, "เ-ือ", Symbol.VOWEL, Symbol.UNKNOWN, Symbol.UNKNOWN,
		 * Symbol.UNKNOWN, Symbol.LONG, "ыыа", "?", "ыыа", "?", null, null, );
		 * Symbol symbol56 = new Symbol(this, "เ-ือ", Symbol.VOWEL,
		 * Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.UNKNOWN, Symbol.LONG, "ууа",
		 * "uua", "ууа", "uua", null, null, );
		 */

		// дифтонги x3
		symbol1.setAdditionalConsonants(new Symbol[] { symbol24 });
		symbol24.setBaseConsonant(symbol1);

		symbol2.setAdditionalConsonants(new Symbol[] { symbol25, symbol26,
				symbol27 });
		symbol25.setBaseConsonant(symbol2);
		symbol26.setBaseConsonant(symbol2);
		symbol27.setBaseConsonant(symbol2);

		symbol3.setAdditionalConsonants(new Symbol[] { symbol28 });
		symbol28.setBaseConsonant(symbol3);

		symbol4.setAdditionalConsonants(new Symbol[] { symbol29, symbol30 });
		symbol29.setBaseConsonant(symbol4);
		symbol30.setBaseConsonant(symbol4);

		symbol9.setAdditionalConsonants(new Symbol[] { symbol31 });
		symbol31.setBaseConsonant(symbol9);

		symbol12.setAdditionalConsonants(new Symbol[] { symbol32 });
		symbol32.setBaseConsonant(symbol12);

		symbol12.setAdditionalConsonants(new Symbol[] { symbol32 });
		symbol32.setBaseConsonant(symbol12);

		symbol35.setAdditionalConsonants(new Symbol[] { symbol36 });
		symbol36.setBaseConsonant(symbol35);

		symbol38.setAdditionalConsonants(new Symbol[] { symbol39 });
		symbol39.setBaseConsonant(symbol38);

		symbol41.setAdditionalConsonants(new Symbol[] { symbol42, symbol43 });
		symbol42.setBaseConsonant(symbol41);
		symbol43.setBaseConsonant(symbol41);

		// lc-hc-mc analogues
		symbol1.setHCAnalogue(symbol34);
		symbol34.setLCAnalogue(symbol1);

		symbol2.setHCAnalogue(symbol35);
		symbol35.setLCAnalogue(symbol2);

		symbol3.setHCAnalogue(symbol37);
		symbol37.setLCAnalogue(symbol3);

		symbol4.setHCAnalogue(symbol38);
		symbol38.setLCAnalogue(symbol4);

		symbol5.setHCAnalogue(symbol40);
		symbol40.setLCAnalogue(symbol5);

		symbol6.setHCAnalogue(symbol41);
		symbol41.setLCAnalogue(symbol6);

		symbol7.setHCAnalogue(symbol44);
		symbol44.setLCAnalogue(symbol7);

		symbols = new Symbol[] { symbol1, symbol2, symbol3, symbol4, symbol5,
				symbol6, symbol7, symbol8, symbol9, symbol10, symbol11,
				symbol12, symbol13, symbol14, symbol15, symbol16, symbol17,
				symbol18, symbol19, symbol20, symbol21, symbol22, symbol23,
				symbol24, symbol25, symbol26, symbol27, symbol28, symbol29,
				symbol30, symbol31, symbol32, symbol33, symbol34, symbol35,
				symbol36, symbol37, symbol38, symbol39, symbol40, symbol41,
				symbol42, symbol43, symbol44, symbol45, symbol46, symbol47,
				symbol48, symbol49, symbol50, symbol51, symbol52, symbol53 };

		consonants = new Symbol[] { symbol1, symbol2, symbol3, symbol4,
				symbol5, symbol6, symbol7, symbol8, symbol9, symbol10,
				symbol11, symbol12, symbol13, symbol14, symbol24, symbol25,
				symbol26, symbol27, symbol28, symbol29, symbol30, symbol31,
				symbol32, symbol33, symbol34, symbol35, symbol36, symbol37,
				symbol38, symbol39, symbol40, symbol41, symbol42, symbol43,
				symbol44, symbol45, symbol46, symbol47, symbol48, symbol49,
				symbol50, symbol51, symbol52, symbol53 };

		vowels = new Symbol[] { symbol15, symbol16, symbol17, symbol18,
				symbol19, symbol20, symbol21, symbol22, symbol23 };

		longVowels = new Symbol[] { symbol15, symbol16, symbol17, symbol18,
				symbol19, symbol20, symbol21, symbol22, symbol23 };

		shortVowels = new Symbol[] {};

		consonants = new Symbol[] { symbol1, symbol2, symbol3, symbol4,
				symbol5, symbol6, symbol7, symbol8, symbol9, symbol10,
				symbol11, symbol12, symbol13, symbol14, symbol24, symbol25,
				symbol26, symbol27, symbol28, symbol29, symbol30, symbol31,
				symbol32, symbol33, symbol34, symbol35, symbol36, symbol37,
				symbol38, symbol39, symbol40, symbol41, symbol42, symbol43,
				symbol44, symbol45, symbol46, symbol47, symbol48, symbol49,
				symbol50, symbol51, symbol52, symbol53 };

		lowConsonants = new Symbol[] { symbol1, symbol2, symbol3, symbol4,
				symbol5, symbol6, symbol7, symbol8, symbol9, symbol10,
				symbol11, symbol12, symbol13, symbol14, symbol24, symbol25,
				symbol26, symbol27, symbol28, symbol29, symbol30, symbol31,
				symbol32, symbol33 };

		middleConsonants = new Symbol[] { symbol45, symbol46, symbol47,
				symbol48, symbol49, symbol50, symbol51, symbol52, symbol53 };

		highConsonants = new Symbol[] { symbol34, symbol35, symbol36, symbol37,
				symbol38, symbol39, symbol40, symbol41, symbol42, symbol43,
				symbol44 };

		baseLowConsonants = new Symbol[] { symbol1, symbol2, symbol3, symbol4,
				symbol5, symbol6, symbol7, symbol8, symbol9, symbol10,
				symbol11, symbol12, symbol13, symbol14 };

		additionalLowConsonants = new Symbol[] { symbol24, symbol25, symbol26,
				symbol27, symbol28, symbol29, symbol30, symbol31, symbol32,
				symbol33 };

		baseLowNotOralAndNazalConsonants = new Symbol[] { symbol1, symbol2,
				symbol3, symbol4, symbol5, symbol6, symbol7 };

		baseLowOralAndNazalConsonants = new Symbol[] { symbol8, symbol9,
				symbol10, symbol11, symbol12, symbol13, symbol14 };

		signToSymbolMap = new HashMap<String, Symbol>(symbols.length);
		for (Symbol symbol : symbols)
			signToSymbolMap.put(symbol.getSign(), symbol);

		this.alphabetCasePackage = new AlphabetCasePackage(this);
	}

	public Symbol getSymbolByUniqueIndex(int index) {
		for (Symbol symbol : symbols) {
			if (symbol.getUniqueIndex() == index)
				return symbol;
		}
		return null;
	}

	public AlphabetCasePackage getCasePackage() {
		return alphabetCasePackage;
	}

	public Symbol getSymbolBySign(String sign) {
		return signToSymbolMap.get(sign);
	}

	public AlphabetConfig getConfig() {
		return config;
	}

	public Symbol[] getAll() {
		return symbols;
	}

	public Symbol[] getConsonants() {
		return consonants;
	}

	public Symbol[] getVowels() {
		return vowels;
	}

	public Symbol[] getLowConsonants() {
		return lowConsonants;
	}

	public Symbol[] getHighConsonants() {
		return highConsonants;
	}

	public Symbol[] getMiddleConsonants() {
		return middleConsonants;
	}

	public Symbol[] getLongVowels() {
		return longVowels;
	}

	public Symbol[] getShortVowels() {
		return shortVowels;
	}

	public Symbol[] getBaseLowConsonants() {
		return baseLowConsonants;
	}

	public Symbol[] getBaseLowNotOralAndNazalConsonants() {
		return baseLowNotOralAndNazalConsonants;
	}

	public Symbol[] getBaseLowOralAndNazalConsonants() {
		return baseLowOralAndNazalConsonants;
	}

	public Symbol[] getAdditionalLowConsonants() {
		return additionalLowConsonants;
	}

}
