package com.iceberg.tlm.alphabet;

import java.io.File;
import java.net.URL;

import com.iceberg.tlm.AlphabetConfig;
import com.iceberg.tlm.Helper;

public class Symbol {

	public static final int NAZAL = 0;

	public static final int ORAL = 1;

	public static final int UNKNOWN = -1;

	public static final int SPIRANT = 0;

	public static final int ASPIRANT = 1;

	public static final int SONORANT = 2;

	public static final int LONG = 0;

	public static final int SHORT = 1;

	public static final int LOW = 0;

	public static final int MIDDLE = 1;

	public static final int HIGH = 2;

	public static final int CONSONANT = 0;

	public static final int VOWEL = 1;

	private int uniqueIndex;

	private int index;

	private String sign;

	private int symbolType;

	private int consonantClass;

	private int consonantPronounceClass;

	private int vowelClass;

	private String pronounciationShortRu;

	private String pronounciationShortEn;

	private String pronounciationFullRu;

	private String pronounciationFullEn;

	private String pronounciationDescr;

	private Alphabet alphabet;

	private Symbol[] additionalConsonants;

	private Symbol lcAnalogue;

	private Symbol hcAnalogue;

	private Symbol mcAnalogue;

	private Symbol baseConsonant;

	private String association;

	public Symbol(int uniqueIndex, Alphabet alphabet, String sign,
			int symbolType, int consonantClass, int consonantPronounceClass,
			int larynxType, int vowelClass, String pronounciationShortRu,
			String pronounciationShortEn, String pronounciationFullRu,
			String pronounciationFullEn, String pronounciationDescr,
			String association, int index) {
		super();
		this.alphabet = alphabet;
		this.sign = sign;
		this.symbolType = symbolType;
		this.consonantClass = consonantClass;
		this.consonantPronounceClass = consonantPronounceClass;
		this.vowelClass = vowelClass;
		this.pronounciationShortRu = pronounciationShortRu;
		this.pronounciationShortEn = pronounciationShortEn;
		this.pronounciationFullRu = pronounciationFullRu;
		this.pronounciationFullEn = pronounciationFullEn;
		this.pronounciationDescr = pronounciationDescr;
		this.association = association;
		this.index = index;
		this.uniqueIndex = uniqueIndex;
	}

	public int getUniqueIndex() {
		return uniqueIndex;
	}

	public int getIndex() {
		return index;
	}

	public String getSign() {
		return sign;
	}

	private String getPronounciationShortRu() {
		return pronounciationShortRu;
	}

	private String getPronounciationShortEn() {
		return pronounciationShortEn;
	}

	private String getPronounciationFullRu() {
		return pronounciationFullRu;
	}

	private String getPronounciationFullEn() {
		return pronounciationFullEn;
	}

	private Alphabet getAlphabet() {
		return alphabet;
	}

	protected void setAdditionalConsonants(Symbol[] additionalConsonants) {
		this.additionalConsonants = additionalConsonants;
	}

	protected void setBaseConsonant(Symbol baseConsonant) {
		this.baseConsonant = baseConsonant;
	}

	protected void setHCAnalogue(Symbol hcAnalogue) {
		this.hcAnalogue = hcAnalogue;
	}

	protected void setMCAnalogue(Symbol mcAnalogue) {
		this.mcAnalogue = mcAnalogue;
	}

	protected void setLCAnalogue(Symbol lcAnalogue) {
		this.lcAnalogue = lcAnalogue;
	}

	public int getSymbolType() {
		return symbolType;
	}

	public boolean isBase() {
		return additionalConsonants == null || additionalConsonants.length == 0;
	}

	public boolean isConsonant() {
		return getSymbolType() == CONSONANT;
	}

	public boolean isVowel() {
		return getSymbolType() == VOWEL;
	}

	private int getLang() {
		return getConfig().getLang();
	}

	private int getPronounciationForm() {
		return getConfig().getPronounciationForm();
	}

	private AlphabetConfig getConfig() {
		return getAlphabet().getConfig();
	}

	private boolean isCompareFormSensitive() {
		return getConfig().isCompareFormSensitive();
	}

	private boolean isCompareLangSensitive() {
		return getConfig().isCompareLangSensitive();
	}

	public boolean isEqualsPronounciation(String pronounciation) {
		if (pronounciation == null)
			return false;
		pronounciation = pronounciation.trim();
		if (pronounciation.length() == 0)
			return false;
		boolean isLS = isCompareLangSensitive();
		boolean isFS = isCompareFormSensitive();
		if (isLS && isFS) {
			return pronounciation.equals(getPronounciation());
		} else if (isLS) {
			return getPronounciationShortForm()
					.equalsIgnoreCase(pronounciation)
					|| getPronounciationFullForm().equalsIgnoreCase(
							pronounciation);
		} else if (isFS) {
			return getPronounciationEn().equalsIgnoreCase(pronounciation)
					|| getPronounciationRu().equalsIgnoreCase(pronounciation);
		}
		return getPronounciationShortEn().equalsIgnoreCase(pronounciation)
				|| getPronounciationShortRu().equalsIgnoreCase(pronounciation)
				|| getPronounciationFullEn().equalsIgnoreCase(pronounciation)
				|| getPronounciationFullRu().equalsIgnoreCase(pronounciation);
	}

	private String getPronounciationShortForm() {
		int lang = getLang();
		if (lang == AlphabetConfig.LANG_EN)
			return getPronounciationShortEn();
		else if (lang == AlphabetConfig.LANG_RU)
			return getPronounciationShortRu();
		else
			throw new UnsupportedOperationException(
					"Unknown language identifier: " + lang);
	}

	private String getPronounciationFullForm() {
		int lang = getLang();
		if (lang == AlphabetConfig.LANG_EN)
			return getPronounciationFullEn();
		else if (lang == AlphabetConfig.LANG_RU)
			return getPronounciationFullRu();
		else
			throw new UnsupportedOperationException(
					"Unknown language identifier: " + lang);
	}

	private String getPronounciationEn() {
		int form = getPronounciationForm();
		if (form == AlphabetConfig.PRONOUNCIATION_FORM_SHORT)
			return getPronounciationShortEn();
		else if (form == AlphabetConfig.PRONOUNCIATION_FORM_LONG)
			return getPronounciationFullEn();
		else
			throw new UnsupportedOperationException(
					"Unknown pronunciation form: " + form);
	}

	private String getPronounciationRu() {
		int form = getPronounciationForm();
		if (form == AlphabetConfig.PRONOUNCIATION_FORM_SHORT)
			return getPronounciationShortRu();
		else if (form == AlphabetConfig.PRONOUNCIATION_FORM_LONG)
			return getPronounciationFullRu();
		else
			throw new UnsupportedOperationException(
					"Unknown pronunciation form: " + form);
	}

	public String getPronounciation() {
		int lang = getLang();
		if (lang == AlphabetConfig.LANG_RU) {
			int pronounciationForm = getPronounciationForm();
			if (pronounciationForm == AlphabetConfig.PRONOUNCIATION_FORM_LONG) {
				return getPronounciationFullRu();
			} else if (pronounciationForm == AlphabetConfig.PRONOUNCIATION_FORM_SHORT) {
				return getPronounciationShortRu();
			}
			throw new UnsupportedOperationException(
					"Unknown pronounciation form: " + pronounciationForm);
		} else if (lang == AlphabetConfig.LANG_EN) {
			int pronounciationForm = getPronounciationForm();
			if (pronounciationForm == AlphabetConfig.PRONOUNCIATION_FORM_LONG) {
				return getPronounciationFullEn();
			} else if (pronounciationForm == AlphabetConfig.PRONOUNCIATION_FORM_SHORT) {
				return getPronounciationShortEn();
			}
			throw new UnsupportedOperationException(
					"Unknown pronounciation form: " + pronounciationForm);
		}
		throw new UnsupportedOperationException("Unknown language identifier: "
				+ lang);
	}

	@Override
	public boolean equals(Object object) {
		return this == object
				|| (object instanceof Symbol && ((Symbol) object).getSign()
						.equals(getSign()));
	}

	@Override
	public String toString() {
		return getSign();
	}

	public URL getSoundInputStream() {
		if (isConsonant()) {
			int index = getIndex();
			return index > 0 ? Helper
					.getInternalResource(Alphabet.CONSONANTS_SOUNDS_DIR
							+ File.separator + getIndex()
							+ Helper.SOUNDS_EXTENSION) : null;
		} else if (isVowel()) {
			int index = getIndex();
			return index > 0 ? Helper
					.getInternalResource(Alphabet.VOWELS_SOUNDS_DIR
							+ File.separator + getIndex()
							+ Helper.SOUNDS_EXTENSION) : null;
		}
		return null;

	}
}
