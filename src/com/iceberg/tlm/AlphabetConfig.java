package com.iceberg.tlm;

public class AlphabetConfig {

	public static final int LANG_RU = 0;

	public static final int LANG_EN = 1;

	public static final int PRONOUNCIATION_FORM_SHORT = 0;

	public static final int PRONOUNCIATION_FORM_LONG = 1;

	private int lang = LANG_RU;

	private int pronounciationForm = PRONOUNCIATION_FORM_LONG;

	private boolean isCompareFormSensitive = true;

	private boolean isCompareLangSensitive = true;

	public AlphabetConfig() {

	}

	public boolean isCompareFormSensitive() {
		return isCompareFormSensitive;
	}

	public void setCompareFormSensitive(boolean isCompareFormSensitive) {
		this.isCompareFormSensitive = isCompareFormSensitive;
	}

	public boolean isCompareLangSensitive() {
		return isCompareLangSensitive;
	}

	public void setCompareLangSensitive(boolean isCompareLangSensitive) {
		this.isCompareLangSensitive = isCompareLangSensitive;
	}

	public int getLang() {
		return lang;
	}

	public void setLang(int lang) {
		this.lang = lang;
	}

	public int getPronounciationForm() {
		return pronounciationForm;
	}

	public void setPronounciationForm(int pronounciationForm) {
		this.pronounciationForm = pronounciationForm;
	}

}
