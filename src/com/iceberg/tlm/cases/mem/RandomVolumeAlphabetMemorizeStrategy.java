package com.iceberg.tlm.cases.mem;

import java.util.Random;

public class RandomVolumeAlphabetMemorizeStrategy extends
		AbstractAlphabetCaseMemorizeStrategy {

	private Random random;

	private Integer[] waste;

	private Integer[] underway;

	@Override
	public void next() {
		updateIndex();
	}

	private void updateIndex() {
		int symbolsSize = getSymbolsSize();

		if (random == null) {
			random = new Random();
			waste = new Integer[symbolsSize];
			underway = new Integer[symbolsSize];
		}

		boolean isWasted = true;
		for (Integer index : underway) {
			if (index != null) {
				isWasted = false;
				break;
			}
		}

		if (isWasted) {
			for (int i = 0; i < symbolsSize; i++) {
				waste[i] = null;
				underway[i] = null;
			}
			for (int i = 0; i < symbolsSize; i++) {
				boolean isContains = true;
				Integer indexCandidate = null;
				while (isContains) {
					indexCandidate = random.nextInt() % symbolsSize;
					indexCandidate = indexCandidate < 0 ? -indexCandidate
							: indexCandidate;

					isContains = false;
					for (Integer curIndex : underway) {
						if (curIndex != null && curIndex == indexCandidate) {
							isContains = true;
							break;
						}
					}
				}
				underway[i] = indexCandidate;
			}
		}

		Integer activeIndex = null;
		for (int i = 0; i < symbolsSize; i++) {
			if (underway[i] == null) {
				activeIndex = underway[i - 1];
				waste[symbolsSize - i] = activeIndex;
				underway[i - 1] = null;
				break;
			} else if (i == symbolsSize - 1) {
				activeIndex = underway[i];
				waste[symbolsSize - i - 1] = activeIndex;
				underway[i] = null;
				break;
			}
		}

		setIndex(activeIndex);
	}

	@Override
	public String getStrategyParams() {
		return super.getStrategyParams() + ":" + getSymbolsSize();
	}

}
