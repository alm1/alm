package com.iceberg.tlm.cases.mem;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import com.iceberg.tlm.alphabet.Symbol;
import com.iceberg.tlm.cases.stats.KeyValueCaseStatistics;

public class SingleInitialAlphabetMemorizeStrategy extends
		AbstractAlphabetCaseMemorizeStrategy {

	public static final int INITITAL_SET_SIZE = 2;

	public static final int INITIAL_MIN_SUCCESS_LIMIT = 5;

	public static final double INITIAL_MIN_MEMORIZE_FACTOR = 0.9;

	public static final int MIN_SUCCESS_LIMIT = 10;

	public static final double MIN_MEMORIZE_FACTOR = 0.8;

	private Set<Integer> processedSet = new HashSet<Integer>();

	private Random random = new Random();

	public void next() {
		if (isNeedNewInvokation()) {
			Integer nextIndex = nextRandom();
			while (processedSet.contains(nextIndex)) {
				nextIndex = nextRandom();
			}
			processedSet.add(nextIndex);
		}
		setIndex(nextRandom());
	}

	private int nextRandom() {
		return Math.abs(random.nextInt() % getSymbolsSize());
	}

	private boolean isNeedNewInvokation() {
		if (getSymbolsSize() > processedSet.size()) {
			KeyValueCaseStatistics stats = getCaseStaticstics();
			int minSuccessLimit = INITITAL_SET_SIZE == processedSet.size() ? INITIAL_MIN_SUCCESS_LIMIT
					: MIN_SUCCESS_LIMIT;
			double minSuccessFactorLimit = INITITAL_SET_SIZE == processedSet
					.size() ? INITIAL_MIN_MEMORIZE_FACTOR : MIN_MEMORIZE_FACTOR;
			for (Integer invokedIndex : processedSet) {
				Symbol symbol = getSymbols()[invokedIndex];
				if (stats.getKeySuccess(symbol) < minSuccessLimit
						|| stats.getSuccessFactor(symbol) < minSuccessFactorLimit)
					return false;

			}
			return true;
		}
		return false;
	}

	@Override
	public void init() {
		for (int i = 0; i < INITITAL_SET_SIZE && i < getSymbolsSize(); i++) {
			processedSet.add(i);
		}
	}

}
