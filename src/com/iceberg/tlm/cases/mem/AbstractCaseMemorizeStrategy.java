package com.iceberg.tlm.cases.mem;

import com.iceberg.tlm.cases.AbstractCase;

public abstract class AbstractCaseMemorizeStrategy {

	private AbstractCase tCase;

	public void setCase(AbstractCase tCase) {
		this.tCase = tCase;
		init();
	}

	protected AbstractCase getCase() {
		return tCase;
	}

	protected void init() {

	}

	abstract public void next();

}
