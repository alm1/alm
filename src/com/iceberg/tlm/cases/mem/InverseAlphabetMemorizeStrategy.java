package com.iceberg.tlm.cases.mem;

public class InverseAlphabetMemorizeStrategy extends
		AbstractAlphabetCaseMemorizeStrategy {

	@Override
	public void next() {
		int index = getIndex();
		if (index < 0 || index + 1 >= getSymbolsSize())
			index = 0;
		else
			index++;
		setIndex(index);
	}

}
