package com.iceberg.tlm.cases.mem;

import java.util.Random;

public class RandomAlphabetMemorizeStrategy extends
		AbstractAlphabetCaseMemorizeStrategy {

	private Random random = new Random();

	private int prev;

	@Override
	public void next() {
		updateIndex();
		if (prev == getIndex())
			updateIndex();
		if (prev == getIndex())
			updateIndex();
		if (prev == getIndex())
			updateIndex();
		if (prev == getIndex())
			updateIndex();
		if (prev == getIndex())
			updateIndex();
	}

	private void updateIndex() {
		setIndex(Math.abs(random.nextInt() % getSymbolsSize()));
	}

}
