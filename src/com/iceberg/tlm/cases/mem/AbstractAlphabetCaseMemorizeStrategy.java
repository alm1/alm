package com.iceberg.tlm.cases.mem;

import com.iceberg.tlm.alphabet.Symbol;
import com.iceberg.tlm.cases.AlphabetCase;
import com.iceberg.tlm.cases.stats.KeyValueCaseStatistics;

public abstract class AbstractAlphabetCaseMemorizeStrategy extends
		AbstractCaseMemorizeStrategy {

	@Override
	protected AlphabetCase getCase() {
		return (AlphabetCase) super.getCase();
	}

	protected int getIndex() {
		return getCase().getIndex();
	}

	protected void setIndex(int index) {
		getCase().setIndex(index);
		updateInverseState();
	}

	protected Symbol[] getSymbols() {
		return getCase().getSymbols();
	}

	protected int getSymbolsSize() {
		return getSymbols().length;
	}

	protected void updateInverseState() {
		getCase().updateInverseState();
	}

	protected KeyValueCaseStatistics getCaseStaticstics() {
		return getCase().getStats();
	}

	public String getStrategyParams() {
		return getClass().getSimpleName();
	}

}
