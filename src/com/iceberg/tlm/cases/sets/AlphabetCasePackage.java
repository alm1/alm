package com.iceberg.tlm.cases.sets;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.cases.AbstractCasePackage;

public class AlphabetCasePackage extends AbstractCasePackage {

	public AlphabetCasePackage(Alphabet alphabet) {
		super(alphabet);
		addCase(new CaseFirst7LCDirect(alphabet));
		addCase(new CaseSecond7LCDirect(alphabet));
		addCase(new CaseThrid10LCDirect(alphabet));
		addCase(new Case10MCDirect(alphabet));
		addCase(new Case10HCDirect(alphabet));
		addCase(new Case9LVDirect(alphabet));
	}

}
