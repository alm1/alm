package com.iceberg.tlm.cases.sets;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.cases.AlphabetCase;

public class Case9LVDirect extends AlphabetCase {

	public Case9LVDirect(Alphabet alphabet) {
		super(
				alphabet,
				"Кейс 6: 9 долгих гласных.",
				"В этом кейсе вы выучите произношения девяти долгих гласных."
						+ "В тесте, вам необходимо угадать правильное произношение буквы на экране и нажать \"проверить и продожить\"."
						+ "Прежде чем начать выполнение просмотрите все буквы этого урока ниже. ",

				alphabet.getLongVowels(), DIRECTION_ONLY_DIRECT);
	}
}
