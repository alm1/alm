package com.iceberg.tlm.cases.sets;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.cases.AlphabetCase;

public class Case10MCDirect extends AlphabetCase {

	public Case10MCDirect(Alphabet alphabet) {
		super(
				alphabet,
				"Кейс 4: 10 средних согласных.",
				"В этом кейсе вы выучите произношения десяти согласных."
						+ " Эти согласные относятся к среднему классу."
						+ "В тесте, вам необходимо угадать правильное произношение буквы на экране и нажать \"проверить и продожить\"."
						+ "Прежде чем начать выполнение просмотрите все буквы этого урока ниже. ",

				alphabet.getMiddleConsonants(), DIRECTION_ONLY_DIRECT);
	}
}
