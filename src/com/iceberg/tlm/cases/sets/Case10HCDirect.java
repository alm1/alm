package com.iceberg.tlm.cases.sets;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.cases.AlphabetCase;

public class Case10HCDirect extends AlphabetCase {

	public Case10HCDirect(Alphabet alphabet) {
		super(
				alphabet,
				"Кейс 5: 10 высоких согласных.",
				"В этом кейсе вы выучите произношения десяти согласных."
						+ " Эти согласные относятся к высокому классу."
						+ "В тесте, вам необходимо угадать правильное произношение буквы на экране и нажать \"проверить и продожить\"."
						+ "Прежде чем начать выполнение просмотрите все буквы этого урока ниже. ",

				alphabet.getHighConsonants(), DIRECTION_ONLY_DIRECT);
	}
}
