package com.iceberg.tlm.cases.sets;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.cases.AlphabetCase;

public class CaseThrid10LCDirect extends AlphabetCase {

	public CaseThrid10LCDirect(Alphabet alphabet) {
		super(
				alphabet,
				"Кейс 3: 10 дублирующих низких согласных.",
				"В этом кейсе вы выучите произношения десяти согласных."
						+ " Эти согласные относятся к низкому классу. Произносятся ровынм тоном. "
						+ "В тесте, вам необходимо угадать правильное произношение буквы на экране и нажать \"проверить и продожить\"."
						+ "Прежде чем начать выполнение просмотрите все буквы этого урока ниже. ",

				alphabet.getAdditionalLowConsonants(), DIRECTION_ONLY_DIRECT);
	}
}
