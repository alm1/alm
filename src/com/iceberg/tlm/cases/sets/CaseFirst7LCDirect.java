package com.iceberg.tlm.cases.sets;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.cases.AlphabetCase;

public class CaseFirst7LCDirect extends AlphabetCase {

	public CaseFirst7LCDirect(Alphabet alphabet) {
		super(
				alphabet,
				"Кейс 1: 7 низких согласных.",
				"В этом кейсе вы выучите произношения семи согласных."
						+ " Эти согласные относятся к низкому классу. Произносятся ровынм тоном. "
						+ "В тесте, вам необходимо угадать правильное произношение буквы на экране и нажать \"проверить и продожить\"."
						+ "Прежде чем начать выполнение просмотрите все буквы этого урока ниже. ",
				alphabet.getBaseLowNotOralAndNazalConsonants(),
				DIRECTION_ONLY_DIRECT);
	}
}
