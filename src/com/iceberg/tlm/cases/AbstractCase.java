package com.iceberg.tlm.cases;

import com.iceberg.tlm.alphabet.Alphabet;

public abstract class AbstractCase {

	private Alphabet alphabet;

	private String name;

	private String description;

	public AbstractCase(Alphabet alphabet, String name, String description) {
		this.name = name;
		this.description = description;
		this.alphabet = alphabet;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Alphabet getAlphabet() {
		return alphabet;
	}

}
