package com.iceberg.tlm.cases;

import java.util.Random;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.alphabet.Symbol;
import com.iceberg.tlm.cases.mem.AbstractAlphabetCaseMemorizeStrategy;
import com.iceberg.tlm.cases.mem.AbstractCaseMemorizeStrategy;
import com.iceberg.tlm.cases.mem.RandomVolumeAlphabetMemorizeStrategy;
import com.iceberg.tlm.cases.stats.KeyValueCaseStatistics;

public class AlphabetCase extends AbstractCase {

	public static final int DIRECTION_ONLY_DIRECT = 0;
	public static final int DIRECTION_ONLY_INVERSE = 1;
	public static final int DIRECTION_DIRECT_AND_INVERSE = 2;

	private Symbol[] symbols;

	private int caseDirection;

	private AbstractCaseMemorizeStrategy memorizeStrategy;

	private int index;

	private Random randomDirection = new Random();

	private boolean isInversedState = false;

	private KeyValueCaseStatistics stats;

	public AlphabetCase(Alphabet alphabet, String name, String description,
			Symbol[] symbols, int caseDirection,
			AbstractCaseMemorizeStrategy memorizeStrategy) {
		super(alphabet, name, description);
		this.symbols = symbols;
		this.caseDirection = caseDirection;
		this.stats = new KeyValueCaseStatistics(symbols);
		this.memorizeStrategy = memorizeStrategy;
		memorizeStrategy.setCase(this);
	}

	public AlphabetCase(Alphabet alphabet, String name, String description,
			Symbol[] symbols, int caseDirection) {
		this(alphabet, name, description, symbols, caseDirection,
				new RandomVolumeAlphabetMemorizeStrategy());
	}

	public String getStrategyParams() {
		return ((AbstractAlphabetCaseMemorizeStrategy) memorizeStrategy)
				.getStrategyParams();
	}

	public Symbol getSymbol() {
		return getSymbols()[getIndex()];
	}

	public String getSign() {
		return getSymbol().getSign();
	}

	public String getPronounciation() {
		return getSymbol().getPronounciation();
	}

	/**
	 * 
	 * Вызыватся для отображения текста, на который пользователь должен дать
	 * ответ-соответсвие
	 * 
	 * @return
	 */
	public String getDirectString() {
		return isInversedState ? getPronounciation() : getSign();
	}

	/**
	 * 
	 * Вызвывается, когда нужно показать правильный ответ
	 * 
	 * @return
	 */
	public String getAssignedString() {
		return isInversedState ? getSign() : getPronounciation();
	}

	/**
	 * 
	 * Вызывеатся, когда нужно узнать, правильный дал ответ пользовать или нет
	 * 
	 * @param response
	 * @return
	 */
	public boolean isRightResponse(String response) {
		return isInversedState ? response.trim().equalsIgnoreCase(getSign())
				: getSymbol().isEqualsPronounciation(response);
	}

	public KeyValueCaseStatistics getStats() {
		return stats;
	}

	public void setWrongResponse() {
		if (isInversedState)
			stats.incInverseError(getSymbol());
		else
			stats.incDirectError(getSymbol());
	}

	public void setRightResponse() {
		if (isInversedState)
			stats.incInverseSuccess(getSymbol());
		else
			stats.incDirectSuccess(getSymbol());
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Symbol[] getSymbols() {
		return symbols;
	}

	/**
	 * 
	 * Если нужен свой алгоритм вычисления индекса, то нужно переопределить этот
	 * метод.
	 * 
	 * @return
	 */
	public void next() {
		memorizeStrategy.next();
	}

	public void updateInverseState() {
		if (caseDirection == DIRECTION_ONLY_DIRECT) {
			if (isInversedState != false)
				isInversedState = false;
		} else if (caseDirection == DIRECTION_ONLY_INVERSE) {
			if (isInversedState != true)
				isInversedState = true;
		} else if (caseDirection == DIRECTION_DIRECT_AND_INVERSE) {
			isInversedState = randomDirection.nextBoolean();
		} else {
			throw new UnsupportedOperationException("Unknown case direction: "
					+ caseDirection);
		}
	}

}
