package com.iceberg.tlm.cases.stats;

public class KeyValueStatistics {

	private Object assignedObject;

	private int directErrors = 0;

	private int inverseErrors = 0;

	private int directSuccess = 0;

	private int inverseSuccess = 0;

	private double startTime;

	private double endTime;

	public KeyValueStatistics(Object assignedObject) {
		this.assignedObject = assignedObject;
	}

	public double getSuccessFactor() {
		int success = getSuccess();
		if (success == 0)
			return 0;
		return success / getTry();
	}

	public int getDirectTry() {
		return getDirectErrors() + getDirectSuccess();
	}

	public int getInverseTry() {
		return getInverseErrors() + getInverseSuccess();
	}

	public int getTry() {
		return getDirectTry() + getInverseTry();
	}

	public int getErrors() {
		return getDirectErrors() + getInverseErrors();
	}

	public int getSuccess() {
		return getDirectSuccess() + getInverseSuccess();
	}

	public void start() {
		startTime = System.currentTimeMillis();
	}

	public void stop() {
		endTime = System.currentTimeMillis();
	}

	public double getTime() {
		return endTime - startTime;
	}

	public Object getAssignedObject() {
		return assignedObject;
	}

	public void incDirectErrors() {
		directErrors++;
	}

	public void incInverseErrors() {
		inverseErrors++;
	}

	public void incDirectSuccess() {
		directSuccess++;
	}

	public void incInverseSuccess() {
		inverseSuccess++;
	}

	public int getDirectErrors() {
		return directErrors;
	}

	public void setDirectErrors(int directErrors) {
		this.directErrors = directErrors;
	}

	public int getInverseErrors() {
		return inverseErrors;
	}

	public void setInverseErrors(int inverseErrors) {
		this.inverseErrors = inverseErrors;
	}

	public int getDirectSuccess() {
		return directSuccess;
	}

	public void setDirectSuccess(int directSuccess) {
		this.directSuccess = directSuccess;
	}

	public int getInverseSuccess() {
		return inverseSuccess;
	}

	public void setInverseSuccess(int inverseSuccess) {
		this.inverseSuccess = inverseSuccess;
	}

	public double getStartTime() {
		return startTime;
	}

	public void setStartTime(double startTime) {
		this.startTime = startTime;
	}

	public double getEndTime() {
		return endTime;
	}

	public void setEndTime(double endTime) {
		this.endTime = endTime;
	}

	public void setAssignedObject(Object assignedObject) {
		this.assignedObject = assignedObject;
	}

}
