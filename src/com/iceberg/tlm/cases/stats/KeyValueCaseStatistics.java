package com.iceberg.tlm.cases.stats;

import java.util.HashMap;
import java.util.Map;

public class KeyValueCaseStatistics {

	private Map<Object, KeyValueStatistics> stats = new HashMap<Object, KeyValueStatistics>();

	private double statrtTime;

	private double endTime;

	public KeyValueCaseStatistics(Object[] assignedObjects) {
		for (Object assignedObject : assignedObjects) {
			stats.put(assignedObject, new KeyValueStatistics(assignedObject));
		}
	}

	public void incDirectError(Object assignedObject) {
		stats.get(assignedObject).incDirectErrors();
	}

	public void incInverseError(Object assignedObject) {
		stats.get(assignedObject).incInverseErrors();
	}

	public void incInverseSuccess(Object assignedObject) {
		stats.get(assignedObject).incInverseSuccess();
	}

	public void incDirectSuccess(Object assignedObject) {
		stats.get(assignedObject).incDirectSuccess();
	}

	public void start() {
		statrtTime = System.currentTimeMillis();
	}

	public void stop() {
		endTime = System.currentTimeMillis();
	}

	public double getTime() {
		return endTime - statrtTime;
	}

	public Map<Object, KeyValueStatistics> getKeyStats() {
		return stats;
	}

	public KeyValueStatistics getKeyStats(Object assignedObject) {
		return getKeyStats().get(assignedObject);
	}

	public int getKeyErrors(Object assignedObject) {
		return getKeyStats(assignedObject).getErrors();
	}

	public int getKeySuccess(Object assignedObject) {
		return getKeyStats(assignedObject).getErrors();
	}

	public double getSuccessFactor(Object assignedObject) {
		return getKeyStats(assignedObject).getSuccessFactor();
	}

	public int getTry(Object assignedObject) {
		return getKeyStats(assignedObject).getTry();
	}

	public int getErrors() {
		int errors = 0;
		for (KeyValueStatistics stat : stats.values())
			errors += stat.getErrors();
		return errors;
	}

	public int getSuccess() {
		int errors = 0;
		for (KeyValueStatistics stat : stats.values())
			errors += stat.getSuccess();
		return errors;
	}

}
