package com.iceberg.tlm.cases;

import java.util.ArrayList;
import java.util.List;

import com.iceberg.tlm.alphabet.Alphabet;

public class AbstractCasePackage {

	private Alphabet alphabet;

	private List<AbstractCase> cases = new ArrayList<AbstractCase>();

	public AbstractCasePackage(Alphabet alphabet) {
		this.alphabet = alphabet;
	}

	protected void addCase(AbstractCase tCase) {
		cases.add(tCase);
	}

	public List<AbstractCase> getCases() {
		return cases;
	}

	public Alphabet getAlphabet() {
		return alphabet;
	}

}
